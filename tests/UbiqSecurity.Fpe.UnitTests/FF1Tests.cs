using System.Diagnostics.CodeAnalysis;
using UbiqSecurity.Fpe.UnitTests.Helpers;
using Xunit.Abstractions;

namespace UbiqSecurity.Fpe.UnitTests
{
	public class FF1Tests
	{
		private readonly ITestOutputHelper _output;

		public FF1Tests(ITestOutputHelper output)
		{
			_output = output;
		}

		[Theory, CsvData(@"TestCases/FF1TestCases.csv", true)]
		[SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Used to show test name/desc in test runner results")]
		public void EncryptDecrypt_ValidInputs_MatchesExpectedOutputs(string name, string base64Key, string base64Tweak, string plainText, string cipherText, int tweakMin, int tweakMax, int? radix, string radixString, int? keyLength)
		{
			Assert.Equal(plainText.Length, cipherText.Length);

			var key = Convert.FromBase64String(base64Key);
			var tweak = Convert.FromBase64String(base64Tweak);
			var alphabet = string.IsNullOrEmpty(radixString) ? FFX.DefaultAlphabet : radixString;

			var testKey = new byte[keyLength ?? key.Length];
			Array.Copy(key, testKey, keyLength ?? key.Length);

			_output.WriteLine($"Original Value: {plainText}");
			_output.WriteLine($"Parameters: radix={radix}, alphabet={alphabet}, tweakMin={tweakMin}, tweakMax={tweakMax}, keyLength={keyLength ?? key.Length}");
			_output.WriteLine($"Key: {Convert.ToBase64String(testKey)} ({testKey.Length})");
			_output.WriteLine($"Tweak: {base64Tweak}");
			var sut = new FF1(testKey, tweak, tweakMin, tweakMax, radix ?? alphabet.Length, alphabet);

			var result = sut.Encrypt(plainText);
			_output.WriteLine($"Encrypted: {result}");
			Assert.Equal(cipherText, result);

			result = sut.Decrypt(cipherText);
			_output.WriteLine($"Decrypted: {result}");
			Assert.Equal(plainText, result);
		}
	}
}