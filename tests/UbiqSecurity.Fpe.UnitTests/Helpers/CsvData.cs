﻿using System.Globalization;
using System.Reflection;
using CsvHelper;
using Xunit.Sdk;

namespace UbiqSecurity.Fpe.UnitTests.Helpers
{
	/// <summary>
	/// Custom xUnit data attribute to pull test data from CSV file
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class CsvData : DataAttribute
	{
		private readonly string _filePath;
		private readonly bool _hasHeaderRow = false;

		public CsvData(string filePath)
		{
			_filePath = filePath;
		}

		public CsvData(string filePath, bool hasHeaderRow)
			: this(filePath)
		{
			_hasHeaderRow = hasHeaderRow;
		}

		public override IEnumerable<object[]> GetData(MethodInfo testMethod)
		{
			var methodParameters = testMethod.GetParameters();
			var parameterTypes = methodParameters.Select(x => x.ParameterType).ToArray();

			using var streamReader = new StreamReader(_filePath);
			using var csvReader = new CsvReader(streamReader, CultureInfo.InvariantCulture);

			if (_hasHeaderRow)
			{
				csvReader.Read();
				csvReader.ReadHeader();
			}

			while (csvReader.Read())
			{
				yield return ParseCsvLine(csvReader, parameterTypes);
			}
		}

		private static object[] ParseCsvLine(CsvReader csvReader, Type[] parameterTypes)
		{
			var values = parameterTypes.Select((x, index) =>
			{
				var stringValue = csvReader.GetField(index);

				object value = x switch
				{
					Type stringType when stringType == typeof(string) => stringValue,
					Type intType when intType == typeof(int) => Convert.ToInt32(stringValue),
					Type nullableIntType when nullableIntType == typeof(int?) => string.IsNullOrEmpty(stringValue) ? null : (int?)Convert.ToInt32(stringValue),
					_ => stringValue
				};

				return value;
			});

			return values.ToArray();
		}
	}
}