﻿using UbiqSecurity.Fpe.Helpers;

namespace UbiqSecurity.Fpe.UnitTests.Helpers
{
	public class BigIntegerHelperTests
	{
		[Theory]
		[InlineData("100", FFX.NumbersAndLowercaseAndUppercaseAlphabet, 62 * 62)]
		[InlineData("z", FFX.NumbersAndLowercaseAndUppercaseAlphabet, 61)]
		[InlineData("zz1", FFX.NumbersAndLowercaseAndUppercaseAlphabet, 1 + (61 * 62) + (61 * 62 * 62))]
		[InlineData("BCDE", "ABCDEFGHIJ", 1234)]
		[InlineData("ABCDE", "ABCDEFGHIJ", 1234)]
		[InlineData("000Dc0RF4", FFX.NumbersAndLowercaseAndUppercaseAlphabet, 12471332306)]

		public void Parse_Tests(string value, string alphabet, long expectedNumber)
		{
			var result = BigIntegerHelper.Parse(value, alphabet);

			Assert.Equal(expectedNumber, (long)result);
		}

		[Theory]
		[InlineData("100", 10, FFX.NumbersAndLowercaseAndUppercaseAlphabet, 100)]
		public void ParseWithRadix_Tests(string value, int radix, string alphabet, int expectedNumber)
		{
			var result = BigIntegerHelper.Parse(value, radix, alphabet);

			Assert.Equal(expectedNumber, (int)result);
		}
	}
}
