﻿using System.Numerics;
using UbiqSecurity.Fpe.Helpers;

namespace UbiqSecurity.Fpe.UnitTests.Helpers
{
	public class BigIntegerExtensionsTests
	{
		[Theory]
		[InlineData(12345, 5, FFX.NumbersAlphabet, "12345")]
		[InlineData(12345, 6, FFX.NumbersAlphabet, "012345")]
		[InlineData(62, 6, FFX.NumbersAndLowercaseAndUppercaseAlphabet, "000010")]
		[InlineData(1 + (61 * 62) + (61 * 62 * 62), 3, FFX.NumbersAndLowercaseAndUppercaseAlphabet, "zz1")]
		[InlineData(12345, 5, "01234567", "30071")]
		[InlineData(12345, 5, "ABCDEFGHIJ", "BCDEF")]
		[InlineData(12345, 6, "ABCDEFGHIJ", "ABCDEF")]
		[InlineData(12345, 7, "ABCDEFGHIJ", "AABCDEF")]
		[InlineData(123, 3, "ȰȱȲȳȴȵȶȷȸȹ", "ȱȲȳ")]
		[InlineData(12471332306, 11, FFX.NumbersAndLowercaseAndUppercaseAlphabet, "00000Dc0RF4")]
		public void ToString_ValidParameters_ReturnsExpectedString(long originalNumber, int desiredStringLength, string alphabet, string expectedStringValue)
		{
			var sut = new BigInteger(originalNumber);

			var result = sut.ToString(desiredStringLength, alphabet);

			Assert.Equal(expectedStringValue, result);
		}

		[Theory]
		[InlineData(12345, 5, 10, FFX.NumbersAndLowercaseAlphabet, "12345")]
		[InlineData(12345, 6, 10, FFX.NumbersAndLowercaseAlphabet, "012345")]
		public void ToStringWithRadix_ValidParameters_ReturnsExpectedString(int originalNumber, int desiredStringLength, int radix, string alphabet, string expectedStringValue)
		{
			var sut = new BigInteger(originalNumber);

			var result = sut.ToString(desiredStringLength, radix, alphabet);

			Assert.Equal(expectedStringValue, result);
		}

		[Fact]
		public void ToString_DesiredLengthSmallerThanStringLength_ThrowsException()
		{
			var sut = new BigInteger(1234);

			Assert.Throws<Exception>(() => sut.ToString(3, 10, "0123456789"));
		}
	}
}
