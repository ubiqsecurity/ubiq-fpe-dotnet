﻿using System.Numerics;
using UbiqSecurity.Fpe.Constants;

namespace UbiqSecurity.Fpe.UnitTests
{
	public class FFXTests
	{
		private static readonly byte[] Key = Convert.FromBase64String("K34VFiiu0qar9xWICc9PPO9DWdjVgKpPfwNtbwT8apQ=");

		[Fact]
		public void Constructor_NullTweak_ThrowsException()
		{
			var ex = Assert.Throws<ArgumentNullException>(() => new FF1(Key, null, 0, 0, 10));

			Assert.Equal("tweak", ex.ParamName);
		}

		[Fact]
		public void Str_NoLeadingZero_Success()
		{
			var expected = "12345";
			var bigInt = new BigInteger(12345);

			var result = FFX.Str(5, 10, bigInt);

			Assert.Equal(expected, result);
		}

		[Fact]
		public void Str_LeadingZero_Success()
		{
			var testString = "012345";
			var bigInt = new BigInteger(012345);

			var s = FFX.Str(6, 10, bigInt);

			Assert.Equal(testString, s);
		}

		[Fact]
		public void Str_PadFront_Success()
		{
			string s;
			var testString = "00012345";
			var bigInt = new BigInteger(0012345);

			s = FFX.Str(8, 10, bigInt);

			Assert.Equal(testString, s);
		}


		[Fact]
		public void Str_TooLong_Fail()
		{
			var bigInt = new BigInteger(012345);

			var ex = Assert.Throws<Exception>(() => FFX.Str(4, 10, bigInt));

			Assert.Equal(FPEExceptionConstants.MaxStringLength, ex.Message);
		}

		[Fact]
		public void Rev_Array_EvenLength_Success()
		{
			var a = new byte[] { 1, 2, 3, 4 };
			var ra = new byte[] { 4, 3, 2, 1 };

			var b = FFX.Rev(a);

			Assert.True(ra.SequenceEqual(b));
		}

		[Fact]
		public void Rev_Array_OddLength_Success()
		{
			var a = new byte[] { 1, 2, 3, 4, 5 };
			var ra = new byte[] { 5, 4, 3, 2, 1 };

			var b = FFX.Rev(a);

			Assert.True(ra.SequenceEqual(b));
		}

		[Fact]
		public void Rev_String_Success()
		{
			var s = "abcd";
			var rs = "dcba";

			var r = FFX.Rev(s);

			Assert.True(rs.SequenceEqual(r));
		}
	}
}
