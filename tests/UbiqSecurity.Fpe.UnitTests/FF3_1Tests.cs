using System.Diagnostics.CodeAnalysis;
using UbiqSecurity.Fpe.UnitTests.Helpers;

namespace UbiqSecurity.Fpe.UnitTests
{
	public class FF3_1Tests
	{
		[Theory, CsvData(@"TestCases/FF3_1TestCases.csv", true)]
		[SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Used to show test name/desc in test runner results")]
		public void EncryptDecrypt_MatchesExpectedOutputs(string name, string base64Key, string base64Tweak, string plainText, string cipherText, int radix, int? keyLength)
		{
			Assert.Equal(plainText.Length, cipherText.Length);

			var key = Convert.FromBase64String(base64Key);
			var tweak = Convert.FromBase64String(base64Tweak);

			var testKey = new byte[keyLength ?? key.Length];
			Array.Copy(key, testKey, keyLength ?? key.Length);

			var sut = new FF3_1(testKey, tweak, radix);
			var result = sut.Encrypt(plainText);
			Assert.Equal(cipherText, result);

			result = sut.Decrypt(cipherText);
			Assert.Equal(plainText, result);
		}
	}
}